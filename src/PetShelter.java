import models.Pet;

public class PetShelter {

    private static int MAX_NUM_PETS = 10;

    // Fields:
    private Pet[] pets;
    private int petCount;

    // Constructor
    public PetShelter() {
        pets = new Pet[MAX_NUM_PETS];
        petCount = 0;

    }

    // Methods:
    void addPet(Pet pet) {
        try {
            this.pets[petCount] = pet;
            petCount++;
        }catch (IndexOutOfBoundsException e){
            System.out.println("Shelter is already full of happy pets:)");
        }

    }

   public Pet searchPet(String searchName) {
        for (int i = 0; i < petCount; i++) {
            if (pets[i].getName().equals(searchName)) {
                return pets[i];
            }
        }

        return null;
    }

    public void removeFromShelter(){

        /* TODO 1st Option:
            To remove an animal from the shelter we will need to go though
            the for or foreach loop and search for pet using an input parameter(the name of the pet)
            (refer to searchPet() method)
            once we found the pet, return its index (its position in the array).
            Now we can use this index to set the petsArray[foundIndex] = null; this way animal will be removed
        */

        /* TODO 2st Option:
            Following same steps as in the Option 1, but perform a deletion using library.
            (You can find it in libs folder, in order to import it to the project on the top
            of the file add an import: import org.apache.commons.lang.ArrayUtils;)
            After that you can apply it.
            Example: ArrayUtils.remove(petsArray, petIndex);
            As you see remove() method requires two parameters an array and the index of the element
            to be removed.
        */

        /* TODO 3rd Option:
            Regarding that the 1st Option is not an effective solution and
            if you want to avoid using library (Option 2), here is a link with the example
            how remove an Element at specific index  from an Array: https://www.geeksforgeeks.org/remove-an-element-at-specific-index-from-an-array-in-java/
         */


    }
}