import models.Cat;
import models.Dog;
import models.Pet;

public class Controller {

    public static void start() {
        fillPetShelter();
    }

    private static void fillPetShelter() {

        PetShelter petShelter = new PetShelter();

        // Create new Dog objects
        Dog lili = new Dog("Lili", 10);
        Dog bella = new Dog("Bella", 12);

        // Create new Cat objects
        Cat kitty = new Cat("Kitty", 10);
        Cat lizzy = new Cat("Lizzy", 9);

        // add pets to the shelter
        petShelter.addPet(lili);
        petShelter.addPet(bella);
        petShelter.addPet(kitty);
        petShelter.addPet(lizzy);

        //searchPet method return an object Pet, so we can assign it to a variable
        //to get some info about the pet we found, for example its cuteness level
        Pet searchedPet = petShelter.searchPet("Bella");
        System.out.println("The cuteness level of " + searchedPet.getName() + " is " + searchedPet.getCutenessLevel());
    }
}
