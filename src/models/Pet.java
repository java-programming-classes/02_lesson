package models;

public class Pet {

    private String name;
    private int cutenessLevel;

    public Pet(String name, int cuteness) {
        this.name = name;
        this.cutenessLevel = cuteness;
    }

    public String getName() {
        if (!(name == null && name.isEmpty()))
            return name;

        return "Make sure the pet's name is valid";
    }

    public int getCutenessLevel() {
        return cutenessLevel;
    }

}
