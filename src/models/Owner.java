package models;

import java.util.Scanner;

public class Owner {

    public static final int MAX_LENGTH = 5;

    private String name;
    private int petsCounter;
    private Pet[] adoptedPets;

    public Owner(String name) {
        this.name = name;
        adoptedPets = new Pet[MAX_LENGTH];
        petsCounter = 0;
    }

    public void adoptPet(Pet pet) {
        try {
            this.adoptedPets[petsCounter] = pet;
            petsCounter++;
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Sorry you can only adopt 5 pets");
        }
    }

    public String selectPet() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter pet's name: ");
        return scanner.nextLine();
    }
}
